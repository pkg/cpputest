Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2007-2011, Michael Feathers, James Grenning and Bas Vodde
License: BSD-3-clause

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2016, Free Software
License: FSFAP

Files: aclocal.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: build/alltests.mmp
 build/bld.inf
 build/cpputest.mmp
Copyright: 2007, Michael Feathers, James Grenning, Bas Vodde and Timo Puronen
License: BSD-3-clause

Files: compile
 depcomp
 missing
 test-driver
Copyright: 1996-2020, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: examples/AllTests/FEDemoTest.cpp
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: include/CppUTest/SimpleMutex.h
Copyright: 2014, Michael Feathers, James Grenning, Bas Vodde and Chen YewMing
License: BSD-3-clause

Files: include/CppUTestExt/IEEE754ExceptionsPlugin.h
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: include/Platforms/*
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ or GPL-3+) with Libtool exception

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR or GPL-2) with Libtool exception

Files: m4/ltversion.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/m4_ax_compiler_vendor.m4
Copyright: 2018, 2019, John Zaitseff <J.Zaitseff@zap.org.au>
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
 2008, Matteo Frigo
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: m4/m4_ax_compiler_version.m4
Copyright: 2014, Bastien ROUCARIES <roucaries.bastien+autoconf@gmail.com>
License: FSFAP

Files: m4/m4_ax_prefix_config_h.m4
Copyright: 2014, Reuben Thomas <rrt@sc3d.org>
 2008, Marten Svantesson
 2008, Guido U. Draheim <guidod@gmx.de>
 2008, Gerald Point <Gerald.Point@labri.fr>
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: platforms/CCStudio/tests/*
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: src/CppUTest/SimpleMutex.cpp
Copyright: 2014, Michael Feathers, James Grenning, Bas Vodde and Chen YewMing
License: BSD-3-clause

Files: src/CppUTestExt/IEEE754ExceptionsPlugin.cpp
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: src/Platforms/Symbian/*
Copyright: 2007, Michael Feathers, James Grenning, Bas Vodde and Timo Puronen
License: BSD-3-clause

Files: tests/CppUTest/SimpleMutexTest.cpp
Copyright: 2014, Michael Feathers, James Grenning, Bas Vodde and Chen YewMing
License: BSD-3-clause

Files: tests/CppUTestExt/IEEE754PluginTest.cpp
 tests/CppUTestExt/IEEE754PluginTest_c.c
 tests/CppUTestExt/IEEE754PluginTest_c.h
Copyright: 2007, 2013, 2015, 2016, Michael Feathers, James Grenning, Bas Vodde
License: BSD-3-clause

Files: Makefile.in
Copyright: 2007-2011, Michael Feathers, James Grenning and Bas Vodde
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE EARLIER MENTIONED AUTHORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
